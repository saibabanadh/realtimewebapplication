
/**
  * Configure yours global variables and urls here
  **/

module.exports = {
	//If local use below
	//mongo_url : 'mongodb://localhost/realtimewebapp'

	//If remote mongodb use below
	mongo_url : 'mongodb://<dbuser>:<dbpassword>@<dbhostingsite>:<port>/realtimewebapp'
}